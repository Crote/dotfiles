# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# Source bash pre-startup files
if [ -d ~/.bashrc-pre.d ]; then
    for filename in ~/.bashrc-pre.d/*; do
        [ -e "$filename" ] || continue
        . "$filename"
    done
fi

# Stop loading all our customization when
# we are not in an interactive session
# Without this, we cannot make an scp connection
# to this account
if [ -z "$PS1" ]; then
    return
fi

# Source bash startup files
if [ -d ~/.bashrc.d ]; then
    for filename in ~/.bashrc.d/*; do
        [ -e "$filename" ] || continue
        . "$filename"
    done
fi
