#/bin/bash
set -e

# Commonly used, but not essential
# TODO: evolution??
if hash flameshot 2>/dev/null; then
	echo "flameshot is installed"
else
	echo "Installing flameshot..."
	sudo dnf install -y flameshot
fi

if hash skypeforlinux 2>/dev/null; then
	echo "skype is installed"
else
	echo "Installing skype..."
	sudo dnf config-manager --add-repo=https://repo.skype.com/rpm/stable/skype-stable.repo
	sudo dnf install skypeforlinux
fi

# Brightness management GUI
if hash gddccontrol 2>/dev/null; then
	echo "ddccontrol-gtk is installed"
else
	echo "Installing ddccontrol-gtk..."
	sudo dnf install -y ddccontrol-gtk
fi
# Brightness management CLI - used together with https://extensions.gnome.org/extension/2645/brightness-control-using-ddcutil/
if hash ddcutil 2>/dev/null; then
	echo "ddcutil is installed"
else
	echo "Installing ddcutil..."
	sudo dnf copr enable rockowitz/ddcutil
	sudo dnf install -y ddcutil
fi

# Entertainment
if hash spotify 2>/dev/null; then
	echo "spotify is installed"
else
	echo "Installing spotify..."
	sudo dnf config-manager --add-repo=https://negativo17.org/repos/fedora-spotify.repo
	sudo dnf install -y spotify-client
fi
if hash vlc 2>/dev/null; then
	echo "vlc is installed"
else
	echo "Installing vlc..."
	sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
	dnf config-manager --add-repo=https://negativo17.org/repos/fedora-spotify.repo
	sudo dnf install -y vlc
fi
if hash openttd 2>/dev/null; then
	echo "openttd is installed"
else
	echo "Installing openttd..."
	sudo dnf install -y openttd
fi
if hash transmission-gtk 2>/dev/null; then
	echo "transmission is installed"
else
	echo "Installing transmission..."
	sudo dnf install -y transmission-gtk
fi
if hash xonotic-glx 2>/dev/null; then
	# Fun little shooter
	echo "xonotic is installed"
else
	echo "Installing xonotic..."
	sudo dnf install -y xonotic
fi

# Rarely used tools - productivity
if hash gummi 2>/dev/null; then
	echo "gummi is installed"
else
	echo "Installing gummi..."
        # TODO: check for texlive individually, gummi seems to need some of its dependencies to work?
	sudo dnf install -y gummi texlive
fi


# Rarely used tools - creativity
if hash gimp 2>/dev/null; then
	echo "gimp is installed"
else
	echo "Installing gimp..."
	sudo dnf install -y gimp
fi
if hash blender 2>/dev/null; then
	echo "blender is installed"
else
	echo "Installing blender..."
	sudo dnf install -y blender
fi
if hash inkscape 2>/dev/null; then
	echo "inkscape is installed"
else
	echo "Installing inkscape..."
	sudo dnf install -y inkscape
fi
if hash entangle 2>/dev/null; then
	echo "entangle is installed"
else
	echo "Installing entangle..."
	sudo dnf install -y entangle
fi
if hash kicad 2>/dev/null; then
	echo "kicad is installed"
else
	echo "Installing kicad..."
	sudo dnf install -y kicad
fi

# Rarely used tools - management
if hash stress 2>/dev/null; then
	# Can be used to stresstest your machine
	echo "stress is installed"
else
	echo "Installing stress..."
	sudo dnf install -y stress
fi
if hash turbostat 2>/dev/null; then
	# used to check cpu utilization / power usage
	echo "kernel-tools is installed"
else
	echo "Installing kernel-tools..."
	sudo dnf install -y kernel-tools
fi
if hash sensors 2>/dev/null; then
	echo "sensors is installed"
else
	echo "Installing sensors..."
	sudo dnf install -y lm_sensors
	echo "NOTE NOTE NOTE: do not forget to run `sudo sensors-detect`!"
fi
if hash d-feet 2>/dev/null; then
	echo "d-feet is installed"
else
	echo "Installing d-feet..."
	sudo dnf install -y d-feet
fi
if hash qdbusviewer-qt5 2>/dev/null; then
	echo "qdbusviewer is installed"
else
	echo "Installing qdbusviewer..."
	sudo dnf install -y qt5-qdbusviewer
fi

if hash seahorse 2>/dev/null; then
	# GUI to manually access passwords stored in gnome
	echo "seahorse is installed"
else
	echo "Installing seahorse..."
	sudo dnf install -y seahorse
fi
if hash dconf-editor 2>/dev/null; then
	echo "dconf-editor is installed"
else
	echo "Installing dconf-editor..."
	sudo dnf install -y dconf-editor
fi
if hash wireshark 2>/dev/null; then
	echo "wireshark is installed"
else
	echo "Installing wireshark..."
	sudo dnf install -y wireshark
fi

# Rarely used tools - other
if hash chromium-browser 2>/dev/null; then
	echo "chromium is installed"
else
	echo "Installing chromium..."
	sudo dnf install -y chromium
fi
