# Tools
## Basic console interaction
- `c`: cd to a commonly-visited directory  
- `cc`: cd to a recently-visited directory

Autocomplete commands, instead of <tab>
- **C-t**: Find a file
- **A-c**: Find a directory
- **C-r**: Find in history
- `cmd`**<Up>**: Find in history for entries starting with `cmd`

- `find`:  Find a file, with preview. Sets var with result.
- `rg`: Search in files. Use as `rg PATTERN [PATH]`

- `mvi SRC`: Move file, prompt for destination.

## Advanced console interaction
- `br`: File management TUI, great to explore directory structures
- `ranger`: File management TUI, with previews

## Basic tooling
- `x`: Extract basically anything
- `hexyl`: Hex file viewer

## Advanced tooling
`htop`, `iotop`
todo: bmon
todo: find more at https://github.com/dustinkirkland/hollywood
todo: ncdu - https://dev.yorhel.nl/ncdu
  can also be done using `br` ?

## Git tooling
`tig`

# Bootstrapping
This dotfile repository comes bundled with a symlink manager. To install the appropriate symlinks, run the following command in the repository root:

```
./manager/.bin/dotfile --dotfiles=$PWD update --dry-run
```
Afterwards, the manager can just be called using `dotfile`.

# Terminal configuration
If you do not use Kitty, it is recommended to set the terminal color scheme to `solarized-dark`.

# Applications
Every application is bundled with a setup file, which will be placed in `~/.setup`. Executing this file will install (the dependencies of) that application and configure basic options not tracked via dotfiles. Setup files are idempotent, so running them multiple times is harmless.

# Possible future improvements
- Broot is not yet setup correctly
  - better alias?
  - hidden stuff not quite working - check dotfiles directory!
- automaticall install fonts? https://www.nerdfonts.com/font-downloads ?
- integrate `z` in `ranger`? https://github.com/skywind3000/z.lua/wiki/FAQ#how-to-integrate-zlua-to-ranger-
- Add `x` reminder for `tar`?
- The in-terminal image preview in `ranger` with `chafa` is currently bugged, see https://github.com/ranger/ranger/issues/1434
  Image preview with `w3mimgdisplay` does not seem to work with Wayland / Konsole / tmux
  `chafa` sort-of works, it's just very ugly
- fix kakm when a non-owned session exists
- fix kakm when files passed contain spaces
- Switch p4merge for subblime merge
- zim might be useful for note-taking https://zim-wiki.org/index.html
- `nnn` might be a `ranger` replacement
- perhaps use `fd` instead of the find alias to fzf, it provides better non-fuzzy searching
   note that the current alias doesnt accept arguments, perhaps rewrite it to call fd if arguments are passed???

- TODO: write a simple script called '?' to find tools / cheat sheets

- WAY better interactive git: https://news.ycombinator.com/item?id=23363767

- Use Regolith i3 as desktop environment: https://regolith-linux.org/

- hex editor: https://github.com/WerWolv/ImHex

# Old notes below

TODO: check out https://news.ycombinator.com/item?id=18898523

TODO: check out `GRV`, tig alternative? or lazygit - https://github.com/jesseduffield/lazygit

https://sneak.berlin/20191011/stupid-unix-tricks/

https://eklitzke.org/lobotomizing-gnome
